import requests

API_KEY = 'trnsl.1.1.20190705T115946Z.45bc02ef0c7a41d8.4612f0dcfa22d05b8ecc4ee01784e1110a6df125'
URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'


def translate_it(from_path, to_path, from_lang, to_lang='ru'):
    """
    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]

    :param from_path:
    :param to_path:
    :param from_lang:
    :param to_lang:
    :return:
    """

    with open(from_path, 'r') as from_file:
        params = {
            'key': API_KEY,
            'text': from_file.read(),
            'lang': '{}-{}'.format(from_lang, to_lang),
        }

        response = requests.get(URL, params=params)
        json_ = response.json()
        text = ''.join(json_['text'])

        with open(to_path, 'w') as to_file:
            to_file.write(text)


translate_it('FR.txt', 'fr_output.txt', 'fr', 'es')
translate_it('ES.txt', 'es_output.txt', 'es')
translate_it('DE.txt', 'de_output.txt', 'de', 'zh')
